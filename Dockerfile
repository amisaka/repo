FROM python:3

LABEL maintainer="Antonio Misaka amisaka@credil.org"
LABEL description="This is a default template for Django"

ENV PYTHONUNBUFFERED 1

RUN apt-get update -y &&     apt-get install -y apt-utils python-pip python-dev vim curl

RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
ADD . /code/

